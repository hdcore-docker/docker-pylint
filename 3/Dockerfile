# HDCore pylint docker image
ARG PYTHON_VERSION="3.12"
FROM python:"${PYTHON_VERSION}-alpine"

ARG PYTHON_VERSION
ARG PYLINT_VERSION="3"
ARG IMAGE_VERSION="${PYLINT_VERSION}"
ARG IMAGE_NAME="hdcore/docker-pylint"
ARG IMAGE_TITLE="HDCore docker-pylint image"
ARG IMAGE_SOURCE="https://gitlab.com/hdcore-docker/docker-pylint"
ARG IMAGE_DESCRIPTION="Docker image based on pylint:${PYLINT_VERSION}"
ARG IMAGE_AUTHORS="Danny Herpol <hdcore@lachjekrom.com>"

ARG CONTAINER_USER='contusr'
ARG CONTAINER_GROUP='contgrp'

# Image labels
LABEL "maintainer"="HDCore" \
    "org.opencontainers.image.title"="${IMAGE_TITLE}" \
    "org.opencontainers.image.description"="${IMAGE_DESCRIPTION}" \
    "org.opencontainers.image.authors"="${IMAGE_AUTHORS}" \
    "org.opencontainers.image.licenses"="MIT" \
    "org.opencontainers.image.url"="https://hub.docker.com/r/${IMAGE_NAME}" \
    "org.opencontainers.image.ref.name"="${IMAGE_NAME}:${IMAGE_VERSION}" \
    "org.opencontainers.image.source"="${IMAGE_SOURCE}"

# Copy scripts and add to the path
COPY scripts/* /usr/local/bin/hdcore/
RUN chmod +x /usr/local/bin/hdcore/*.sh
ENV PATH="/usr/local/bin/hdcore:${PATH}"

# Upgrade base image
RUN apk -U upgrade

# Add normal user and group
RUN addgroup -S "${CONTAINER_GROUP}" && adduser -S "${CONTAINER_USER}" -G "${CONTAINER_GROUP}"
USER ${CONTAINER_USER}

# Install pylint version
COPY ${IMAGE_VERSION}/requirements.txt /tmp/
RUN pip install --user --upgrade --no-cache-dir -r /tmp/requirements.txt
ENV PATH="/home/${CONTAINER_USER}/.local/bin:${PATH}"

# Set workdir
WORKDIR /code

# Entrypoint and command
ENTRYPOINT [ "docker-entrypoint.sh" ]
CMD ["pylint", "."]
