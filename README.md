# HDCore - docker-pylint

## Introduction

This is a small container image that contains a python3 environment with pylint3. This container is very usefull for automatic testing during CI.

## Image usage

- Run pylint:

```bash
docker run --rm -v /path/to/python/code:/code hdcore/docker-pylint:<version> pylint <arguments>
```

- Run shell:

```bash
docker run -it --rm -v /path/to/python/code:/code hdcore/docker-pylint:<version> /bin/sh
```

- Use in .gitlab-ci.yml:

```bash
image: hdcore/docker-pylint:<version>
script: pylint <arguments>
```

## Available tags

- hdcore/docker-pylint:3

## Container Registries

The image is stored on multiple container registries at dockerhub and gitlab:

- Docker Hub:
  - hdcore/docker-pylint
  - registry.hub.docker.com/hdcore/docker-pylint
- Gitlab:
  - registry.gitlab.com/hdcore-docker/docker-pylint

## Building image

Build:

```bash
docker build -f .\3\Dockerfile -t docker-pylint:3 .
```
